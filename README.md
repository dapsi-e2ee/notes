# E2EE Notes

### How to set up Notes app in murena cloud?

1. If Notes app is already installed/enabled in murena cloud, remove it by going to /settings/apps/installed/notes and click disable/remove
2. Download the package from app releases section https://gitlab.e.foundation/dapsi-e2ee/notes/-/releases/v1.0.0
3. Download the file in server and untar it
4. Move it to html/custom_apps and set ownership to www-data user using chown
5. Go to apps section and enable Notes app


### How does notes app in murena cloud work?

Notes app use mailvelope and OpenPGP.js to encrypt and decrypt Note files. mailvelope manages key generation, key storing, and access management. Notes application access these keys from mailvelope storage and performs encryption and decryption of files.

### Improvements:

* Remove the need to use mailvelope browser xtension
* make encryption and decryption work with keys generate by E2E Encryption app

### Building the app

1. Clone this into your `apps` folder of your Nextcloud
2. In a terminal, run the command `make dev-setup` to install the dependencies
3. Then to build the Javascript run `make build-js` or `make watch-js` to
   rebuild it when you make changes
4. Enable the app through the app management of your Nextcloud


### REST API for third-party apps

The notes app provides a JSON-API for third-party apps. Please have a look at our **[API documentation](docs/api/README.md)**.
